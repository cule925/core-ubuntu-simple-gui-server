package cul3.core;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import javax.imageio.ImageIO;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import java.lang.Runnable;

public class Server {

	private static AtomicBoolean ready = new AtomicBoolean(false);	//Dretveno sigurna varijabla koja govori da je GUI namjesten
	private static SSLServerSocket sslServerSocket;					//SSLServerSocket
	
	private static List<DisplayUnit> devices = Collections.synchronizedList(new LinkedList<DisplayUnit>());	//Dretveno sigurni niz mjesta za uredjaje u GUI-u
	private static int MAX_DEVICES = 6;
	
	private static String imagePath = "src/main/resources/icons/";
	private static String[] imageName = {"Empty.png", "Fridge.png", "Lamp.png", "Thermometer.png", "Toaster.png", "TV.png"};
	
	public static void main(String[] args) {
		
		System.out.println("Starting server...");
		String userLinux = System.getProperty("user.name");
		String pathToSSLServerInfo = "/home/" + userLinux + "/ssl/server/";
		
		/*SSL postavke*/
		System.setProperty("javax.net.ssl.trustStore", pathToSSLServerInfo + "serverTrustStore.jts");
		System.setProperty("javax.net.ssl.keyStoreType", "jks");
		System.setProperty("javax.net.ssl.keyStore", pathToSSLServerInfo + "serverKeyStore.jks");
		System.setProperty("javax.net.ssl.keyStorePassword", "password");
		System.setProperty("javax.net.ssl.trustStorePassword", "password");
		
		/*Zapocni GUI dretvu*/
		SwingUtilities.invokeLater(new Runnable() {
			
			public void run() {
			
				KioskFrame frame = new KioskFrame();
				frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
				frame.setExtendedState(JFrame.MAXIMIZED_BOTH);						//Rasiri maksimalno prozor
				frame.setUndecorated(true); 										//Makni konstante prozora (fullscreen) 
				frame.getContentPane().setBackground(new Color(16777215));			//Boja pozadine
				frame.setVisible(true);
	
			}
		
		});
		
		/*Provjeravaj jeli GUI namjesten*/
		while(!ready.get());
		
		try {
			
			SSLServerSocketFactory sslServerSocketFactory = (SSLServerSocketFactory)SSLServerSocketFactory.getDefault();
			sslServerSocket = (SSLServerSocket)sslServerSocketFactory.createServerSocket(49152);
			
			/*Autentifikacija i klijenta, uz autentifikaciju posluzitelja*/
			sslServerSocket.setNeedClientAuth(true);
			
			/*Prisluskuj nadolazece zahtjeve i stvori nove dretve za odgovor na zahtjeve*/
			while(true) {
				
				SSLSocket socket = (SSLSocket)sslServerSocket.accept();
				DeviceHandler deviceHandler = new DeviceHandler(socket);
				deviceHandler.start();
				
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Socket config. error!");
			System.exit(1);
		}
		
		System.out.println("Server finished...");
		
	}
	
	/*Odgovor na zahtjev uredjaja odnosno dretva*/
	private static class DeviceHandler extends Thread {
		
		/*Podatci o uredjaju*/
		private String devId;
		private String devType;
		private SSLSocket sslSocket = null;
		private String remoteSocketAddress;
		
		/*Slanje i primanje podataka*/
		private InputStreamReader inputStreamReader = null;
		private OutputStreamWriter outputStreamWriter = null;
		private BufferedReader bufferedReader = null;
		private BufferedWriter bufferedWriter = null;
		
		/*Mjesto na GUI-u*/
		private boolean takenPlaceOnScreen = false;
		private int pointerOnPlace = -1;
		
		private static AtomicInteger placesHeld = new AtomicInteger(0); /*Dretveno sigurna varijabla, broji broj uredjaja na GUI-u (maks. 6)*/
		
		public DeviceHandler(SSLSocket sslSocket) {
			
			this.sslSocket = sslSocket;
			this.remoteSocketAddress = sslSocket.getRemoteSocketAddress().toString().substring(1);
			
		}
		
		/*Odgovor na zahtjev uredjaja (dretva) pri instaciranju pocinje izvoditi ovu metodu*/
		@Override
		public void run() {
			
			try {
				
				inputStreamReader = new InputStreamReader(sslSocket.getInputStream());
				outputStreamWriter = new OutputStreamWriter(sslSocket.getOutputStream());
				
				bufferedReader = new BufferedReader(inputStreamReader);
				bufferedWriter = new BufferedWriter(outputStreamWriter);
			
				/*Citaj iz medjuspremnika dok konekcija nije prekinuta*/
				boolean shutConnection = false;
				while(!shutConnection) {
					
					/*Ako ima praznih mjesta na GUI-u, uzmi mjesto*/
					if(!takenPlaceOnScreen && devId != null) takePlace();
					
					/*Citaj liniju u medjuspremniku*/
					String msg = bufferedReader.readLine();
					
					if(msg.contains("DATA")) printDevInfo(msg); //Ako ima podataka za prikazati
					else if(msg.equals("DISCONNECTED")) {		//Ako se uredjaj iskljucio
						
						shutConnection = true;
						System.out.println("Device: " + devType + " (" + devId + ") @" + remoteSocketAddress + " disconnected!");
						
					} else if(msg.contains("DEVINFO")) {		//Pocetne informacije o uredjaju
						
						String[] info = msg.split("\\|");
						devId = info[1];
						devType = info[2];

						System.out.println("Device: " + devType + " (" + devId + ") @" + remoteSocketAddress + " connected!");
						
					} 
					
				}
				
				/*Ako se uredjaj iskljucio sa mreze makni ga sa GUI-a*/
				printDisconnected();
				
				/*Oslobodi mjesto na GUI-u*/
				if(takenPlaceOnScreen) releasePlace();
				
				bufferedWriter.close();
				bufferedReader.close();
				outputStreamWriter.close();
				inputStreamReader.close();
				
				sslSocket.close();
				
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Socket config. error!");
				System.exit(1);
			}		
				
		}
		
		/*Dretveno sigurna metoda, uzima uredjaju mjesto na GUI-u (ako ima mjesta)*/
		private synchronized void takePlace() {
			
			if(placesHeld.get() < MAX_DEVICES ) {
				for(int i = 0; i < MAX_DEVICES; i++) {
					DisplayUnit unit = devices.get(i);
					if(unit.thread == null) {
						unit.thread = Thread.currentThread();
						takenPlaceOnScreen = true;
						pointerOnPlace = i;
						placesHeld.incrementAndGet();
						break;
					}
				}
			}
		
		}
		
		/*Dretveno sigurna metoda, uklanja uredjaj sa mjesta na GUI-u*/
		private synchronized void releasePlace() {
			
				for(int i = 0; i < MAX_DEVICES; i++) {
					DisplayUnit unit = devices.get(i);
					if(unit.thread == Thread.currentThread()) {
						unit.thread = null;
						takenPlaceOnScreen = false;
						pointerOnPlace = -1;
						placesHeld.decrementAndGet();
						break;
					}
				}
			
		}
		
		/*Ispisi podatke na GUI*/
		private void printDevInfo(String msg) {
			
			if(takenPlaceOnScreen) {
				
				String[] data = msg.split("\\|");
				String dataAssembled = "";
				
				for(int i = 1; i < data.length; i++) dataAssembled += data[i] + "<br/>";
				
				JLabel text = devices.get(pointerOnPlace).text;
				JLabel picture = devices.get(pointerOnPlace).picture;
				String info = "<html>ID:" + devId + "<br/>DEVICE TYPE:" + devType + "<br/>@" + remoteSocketAddress + "<br/>" + dataAssembled + "</html>";
				text.setText(info);
				
				BufferedImage image = null;
				try {
					int pointer = determineImage();
					image = ImageIO.read(new File(imagePath + imageName[pointer]));
				} catch(IOException e) {
					e.printStackTrace();
					System.out.println("Error opening icon!");
				}
				
				picture.setIcon(new ImageIcon(image));
				
			}
			
		}
		
		/*Ispisi da se uredjaj uklonio sa mreze*/
		private void printDisconnected() {
			
			if(takenPlaceOnScreen) {
			
				JLabel text = devices.get(pointerOnPlace).text;
				JLabel picture = devices.get(pointerOnPlace).picture;
				String info = "<html>NO DEVICE CONNECTED</html>";
				text.setText(info);
			
				BufferedImage image = null;
				try {
					image = ImageIO.read(new File(imagePath + imageName[0]));
				} catch(IOException e) {
					e.printStackTrace();
					System.out.println("Error opening icon!");
				}
			
				picture.setIcon(new ImageIcon(image));
			
			}
			
		}
		
		/*Odredi ikonu ovisno o tipu uredjaja*/
		private int determineImage() {
			
			if(devType.equals("FRIDGE")) return 1;
			else if(devType.equals("LAMP")) return 2;
			else if(devType.equals("THERMOMETER")) return 3;
			else if(devType.equals("TOASTER")) return 4;
			else if(devType.equals("TV")) return 5;
			else return 0;
			
		}
		
	}
	
	private static class KioskFrame extends JFrame {
		
		private static final long serialVersionUID = 1L;

		public KioskFrame() {
			
			/*Inicijaliziraj pozadinski panel borderlayout tipa*/
			JPanel panel = new JPanel();
			panel.setLayout(new BorderLayout());
			
			initializeComponents(panel);
			
		}
		
		private void initializeComponents(JPanel panel) {
			
			/*Dodaj pozadinski panel*/
			this.add(panel);
			
			/*Na gornjem dijelu pozadinskog panela dodaj panel flowlayout (default) tipa cijan pozadine i tekstom*/
			JLabel labelNorth = new JLabel("DEVICES CONNECTED");
			
			labelNorth.setVerticalAlignment(JLabel.CENTER);
			labelNorth.setForeground(new Color(16777215));
			labelNorth.setFont(new Font("Calibri", Font.BOLD, 50));
			
			JPanel panelNorth = new JPanel();
			
			panelNorth.add(labelNorth);
			panelNorth.setBackground(new Color(52428));
			panelNorth.setPreferredSize(new Dimension(600, 100));
	        panelNorth.setMinimumSize(new Dimension(600, 100));
	        
	        panel.add(panelNorth, BorderLayout.NORTH);
			
	        
	        /*Na sredisnjem dijelu pozadinskog panela dodaj panel gridlayout tipa bijele pozadine (mreza 2 retka 3 stupca)*/
			JPanel panelCenter = new JPanel(new GridLayout(2, 3));
			
			panelCenter.setBackground(new Color(16777215));
			
			panel.add(panelCenter, BorderLayout.CENTER);
			
			/*Dodaj komponente na sredisnji panel*/
			addGridComponents(panelCenter);
			
			/*Dretveno sigurno objavi spremnost GUI-a*/
			ready.set(true);
			
		}

		private void addGridComponents(JPanel panelCenter) {
			
			/*Puni celije na sredisnjem panelu*/
			for(int i = 0; i < MAX_DEVICES; i++) {
				JPanel jPanel = new JPanel(new GridLayout(2, 1)); //Sama celija je gridlayout (mreza 2 retka 1 stupac)
				jPanel.setBackground(new Color(16777215));		  //Bijela boja	
				devices.add(new DisplayUnit(jPanel));			  //Referenciraj panel u instanci DisplayUnit razreda
				panelCenter.add(jPanel);
			}
			
			/*Popuni podatke u celiji*/
			for(DisplayUnit device : devices) deviceCharacteristics(device);			
			
		}
		
		private void deviceCharacteristics(DisplayUnit device) {
			
			BufferedImage image = null;
			
			/*Iscitaj default-nu sliku*/
			try {
				image = ImageIO.read(new File(imagePath + imageName[0]));
			} catch(IOException e) {
				e.printStackTrace();
				System.out.println("Error opening icon!");
			}
			
			/*Postavi sliku u jednu celiju*/
			JLabel picture = new JLabel(new ImageIcon(image));
			picture.setVerticalAlignment(JLabel.BOTTOM);
			
			/*Postavi tekst u jednu celiju*/
			JLabel text = new JLabel("<html>NO DEVICE CONNECTED</html>");
			text.setHorizontalAlignment(JLabel.CENTER);
			text.setVerticalAlignment(JLabel.TOP);
			text.setFont(new Font("Calibri", Font.BOLD, 20));
			
			/*Referenciraj referencu slike i teksta u instanci DisplayUnit razreda*/
			device.picture = (picture);
			device.text = text;
			device.initUnit();
			
		}
		
	}
	
	private static class DisplayUnit {
		
		private JPanel panel;
		private JLabel picture = null;
		private JLabel text = null;
		private Thread thread = null;
		
		public DisplayUnit(JPanel panel) {
			
			this.panel = panel;
			
		}
		
		private void initUnit() {
			panel.add(picture);
			panel.add(text);
		}
		
	}
	
}
